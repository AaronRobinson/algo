const sleep = require("./sleep");
const { price, BITCOIN } = require("./exchange");

// Think about this as a formula in Excel
async function request() {
  // do whatever is between the braces forever
  do {
    // get the price from the exchange
    let { bid, ask } = await price(BITCOIN);

    // log the bid, ask and spread to the console
    console.log(`bid:${bid} ask:${ask} spread:${Math.abs(ask - bid)}`);

    await sleep(2); // don't upset Bitfinex's API rate limiting
  } while (true); // forever
}

// call the formula
request();
