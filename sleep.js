module.exports = (duration) =>
  new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, duration * 1000);
  });
