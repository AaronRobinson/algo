const sleep = require("./sleep");
const { price, BITCOIN } = require("./exchange");

/**
 * My algo is super simple
 * If you don't have Bitcoin 'buy' one (I don't really - I just record the ask price)
 * Check the price every 2 seconds
 * If the bid goes above the strike cash out
 * And repeat until you get a margin call! (you could code it to stop after making x profit)
 */

async function request() {
  let bitcoin = false;
  let strike;
  let account = 10;

  do {
    let { bid, ask } = await price(BITCOIN);

    // TODO buy bitcoin if we don't have any (!bitcoin bitcoin=true,strike=ask)

    // TODO take profits if we're in the money (bid>strike?account+=bid-strike,bitcoin=false,position=0:position=bid-strike)
    // TODO monitor the position otherwise

    // The spread is output for info but also important as a wide spread could trigger a margin call
    const spread = ask - bid;

    // log the bid, ask and spread to the console
    console.log(
      `bid:${bid} ask:${ask} spread:${spread} strike:${strike} account:${account} position:${position}`
    );

    // TODO Margin call (account + position < 0)

    await sleep(2);
  } while (true);
}

request();
