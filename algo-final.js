const sleep = require("./sleep");
const { price, BITCOIN } = require("./exchange");

/**
 * My algo is super simple
 * If you don't have Bitcoin 'buy' one (I don't really - I just record the ask price)
 * Check the price every 2 seconds
 * If the bid goes above the strike cash out
 * And repeat until you get a margin call! (you could code it to stop after making x profit)
 */

async function request() {
  let account = 10; // represents an account holding £
  let bitcoin = false; // whether we're holding bitcoin
  let strike;

  do {
    let { bid, ask } = await price(BITCOIN);

    // ALGO STARTS
    if (!bitcoin) {
      // if we don't have any coins we record the ask price to represent what we would have bought at
      strike = ask;
      // then we set a flag just to denote that we're now holding bitcoin
      bitcoin = true;
      console.log("FOMO - Buy Bitcoin!");
    }

    if (bid > strike) {
      // close the position if we're in the money
      bitcoin = false;
      account += bid - strike; // add the profits to our account
      position = 0; // reset the position
    } else {
      position = bid - strike; // calculate the position as what it could cost me to close it against the bid
    }

    // The spread is output for info but also important as a wide spread could trigger a margin call
    const spread = ask - bid;

    console.log(
      `bid:${bid} ask:${ask} strike: ${strike} spread:${spread} account:${account} position:${position}`
    );

    // This will happen eventually with this algorithm
    if (account + position < 0) {
      console.log("OHNO - margin call!!!!");
      process.exit(0);
    }
    // ALGO ENDS

    await sleep(2);
  } while (true);
}

request();
