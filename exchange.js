const fetch = require("node-fetch");

module.exports = {
  BITCOIN: "BTCGBP",
  price: async (pair) => {
    const response = await fetch(
      `https://api-pub.bitfinex.com/v2/ticker/t${pair}`
    );
    const [bid, bidSize, ask] = await response.json();
    return { bid, ask };
  },
};
