# Background

1, This is a mock algo trading bot that uses the public Bitfinex BTCGBP price to make decisions about when to buy or sell

2, This walkthrough is intended to demystify engineering by exposing you some of everyday tools we use use and you'll hear us talk about:
 - Code repos (Git)
 - APIs and how you can use them in your browser and in code
 - Endpoints

3, We'll use the Bitfinex exchange

Home page https://www.bitfinex.com/
BTCGBP page https://trading.bitfinex.com/t/BTC:GBP
Without an API you would interact with the site using the buy and sell buttons
With an APIs you can trade using code 
To find the APIs https://docs.bitfinex.com/docs
Public endpoints https://docs.bitfinex.com/docs/rest-public
All tickers endpoint https://api-pub.bitfinex.com/v2/tickers?symbols=ALL
BTCGBP ticker endpoint (for the price) https://api-pub.bitfinex.com/v2/ticker/tBTCGBP

4, Code to track BTCGBP price

link here

5, Set up a mock algo trading bot

## Install NodeJS if you don't have it

The code is JavaScript and we'll run it using NodeJS
Check if you have NodeJS `node --version`
If not, install it by following the instructions at `https://nodejs.org/en/download/`

## Clone the repo

Open a shell/terminal (on Mac use terminal if you don't have another tool)
Change to your home directory `cd ~`
Clone the repo (copy the code to your machine) `git clone https://gitlab.com/AaronRobinson/algo.git`
Change into the cloned repo `cd algo`

## Install the libraries

Install the libraries `npm i`

## Run the algo

`node algo`

## HEALTH WARNING!!

This is addictive - go and write your own algo!

## This is the best I've seen...
Starting from £10 balance, taking profits up to a balcne of £434 before it was gradually eroded by losses
```
...
bid:35331 ask:35332 strike: 35749 spread:1 account:434 position:-418
bid:35331 ask:35332 strike: 35749 spread:1 account:434 position:-418
bid:35267 ask:35283 strike: 35749 spread:16 account:434 position:-482
```
